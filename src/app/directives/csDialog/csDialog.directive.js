/**
 * Created by fbferreira on 25/01/2017.
 */
'use strict';

/* Diretiva para ....
 * TODO documentacao
 * */

function csDialog (SweetAlert) {
    return {
        restrict: 'A',
        scope: {
            titulo: '=',
            texto: '=',
            funcao: '&executar'
        },
        link: function(scope, element, attrs) {

            scope.mostrarBotaoCancelar = true;
            scope.textoBotaoCancelar = "Não";
            scope.textoBotaoConfirmar = "Sim";
            scope.colorBtnPrimary = "#0d426b";
            scope.icone = "warning";

            element.on('click', function (e) {
                scope.$apply(function () {

                    SweetAlert.swal({
                            title: scope.titulo,
                            text: scope.texto,
                            type: scope.icone,
                            showCancelButton: scope.mostrarBotaoCancelar,
                            cancelButtonText: scope.textoBotaoCancelar,
                            confirmButtonColor: scope.colorBtnPrimary,
                            confirmButtonText: scope.textoBotaoConfirmar,
                            closeOnConfirm: true
                        },
                        function (confirm) {
                            if (confirm) {
                                scope.funcao();
                            }
                        });

                });
            });
        }
    };
};

export default csDialog;