'use strict';
import csDialogDirective from './csDialog.directive';

var m = angular.module('csDialog', [])
    .directive('csDialog', csDialogDirective);

export default m;