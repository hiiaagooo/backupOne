# dataBr

## Descrição
Aplica a formatação dd/mm/yyyy.

Validações realizadas:
* Ano mínimo permitido é 1.900
* Somente númericos são aceitos
* Mês de Fevereiro com dia até 29

## Como usar
* No HTML adicionar a tag: `mask-data-br`

#### Parâmetros

Não há.