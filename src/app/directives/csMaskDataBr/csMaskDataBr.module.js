'use strict';
import csMaskDataBrDirective from './csMaskDataBr.directive.js';

var m = angular.module('csMaskDataBr', [])
    .directive('csMaskDataBr', csMaskDataBrDirective);

export default m;
