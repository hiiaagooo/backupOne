'use strict';

var StringMask = require('string-mask');
var maskFactory = require('../../../../node_modules/angular-input-masks/src/node_modules/mask-factory');

var numeroProcessoSei = new StringMask('00\/00\/0000');

module.exports = maskFactory({
    clearValue: function (rawValue) {
        return rawValue.replace(/[^\d]/g, '').slice(0, 9);
    },
    format: function (cleanValue) {
        var formatedValue;
        formatedValue = numeroProcessoSei.apply(cleanValue);
        // console.log(formatedValue)
        return formatedValue.trim().replace(/[^0-9]$/, '');
    },
    validations: {
        databr: function (value) {
            value = value.replace(/[^\d]/g, '').slice(0, 9);
            var m30 = ['04','06','09','11'];
            var dd = value.substr(0,2);
            var mm = value.substr(2,2);
            var yyyy = value.substr(4,4);

            switch (true){
                case value.length < 7: return false; break;
                case yyyy < 1900: return false;
                case dd == 0 || mm == 0: return false; break;
                case dd > 29 && mm == 2: return false; break;
                case dd > 30 && m30.indexOf(mm) != -1: return false; break;
                case dd > 31 || mm > 12: return false; break;
                default: return true;
            }

        }
    }
});
