'use strict'
import csMessageTemplate from './csMessage.template.html';
import csMessageService from "./csMessage.service";

function csMessage() {
    return {
        restrict: 'E',
        controller: ['$scope', 'csMessageService', function ($scope, csMessageService) {
            $scope.messageController = csMessageService;
        }],
        templateUrl: csMessageTemplate
    };
};

export default csMessage;