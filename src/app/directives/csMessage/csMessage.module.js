import csMessageDirective from './csMessage.directive'
import csMessageService from './csMessage.service'

export default angular.module('csMessage', [])
    .directive('csMessage', csMessageDirective)
    .service('csMessageService', csMessageService);