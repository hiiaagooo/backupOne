'use strict'
function CsMessageService($timeout) {

    var vm = this;
    vm.type = 0;
    vm.messages = [];
    vm.show = false;
    vm.SUCCESS = 0;
    vm.ERROR = 1;
    vm.ALERT = 2;
    vm.INFO = 3;

    vm.removeMessage = function (message) {
        var lista = [];

        for (var i = 0; i < vm.messages.length; i++) {
            if (vm.messages[i].position != message.position) {
                lista.push(vm.messages[i]);
            }
        }
        vm.messages = angular.copy(lista);
    };

    vm.setMessage = function (text, type) {
        vm.type = type;
        var message = {
            text: text,
            type: type,
            position: vm.messages.length == 0 ? 0 : (vm.messages[vm.messages.length - 1].position) + 1
        }
        vm.messages.push(message);

        $timeout(function () {
            vm.removeMessage(message)
        }, 10000);
    };

    return vm;
}

CsMessageService.$inject = ['$timeout']

module.exports = CsMessageService;