'use strict';
import csMultiSelectDirective from './csMultiSelect.directive';

var m = angular.module('csMultiSelect', [])
    .directive('csMultiSelect', csMultiSelectDirective);

export default m;