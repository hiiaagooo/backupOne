/**
 * Created by fbferreira on 06/12/2017.
 */
'use strict';
import csMultiSelectTemplate from './csMultiSelect.template.html';
/* Diretiva para movimentar dados de uma lista (A) para outra lista (B)
 * Sendo que:
 *   1 - os elementos transferidos para a listaB serao retirados da listaA (movimentar, não copiar)
 *   2 - os dados presentes na listaB serao mesclados com os novos dados
 *
 *   Modo de usar:
 *    <div id="nomeDaDiv"
 *    cs-multi-select                                   -- chamada/incorporar a diretiva
 *    lista-a="vm.cidades"                          -- lista com as opcoes para o ator
 *    label-lista-a="'Cidades'"                     -- titulo da primeira lista
 *    lista-b="vm.cidadesSelecionadas"              -- lista com as opcoes escolhidas pelo ator
 *    label-lista-b="'Cidades Selecionadas'"        -- titulo da segunda lista
 *    exibir="'nome_cidade'"                        -- nome do atributo a ser apresentado para o ator
 *    ></div>
 * */
function csMultiSelect () {
    return {
        scope: {
            listaA: '=',
            labelListaA: '=',
            listaB: '=',
            labelListaB: '=',
            exibir: '='
        },
        link: function(scope, element, attrs) {
            scope.directionListAToListB = true;

            scope.elementToAdd = "";
            scope.elementToDelete = "";

            scope.doMoveOneElement = function (directionListAToListB) {
                const ELEMENT_NOT_FOUND = -1;

                if (directionListAToListB) {
                    if (scope.elementToAdd) {
                        let indexOfItem = scope.listaA.findIndex(function retornaIndex(item) {
                            return angular.equals(item, scope.elementToAdd[0]);
                        })
                        if (indexOfItem != ELEMENT_NOT_FOUND) {
                            let element = scope.listaA.splice(indexOfItem, 1);
                            scope.listaB.push(element[0]);
                        }
                    }
                } else {
                    if (scope.elementToDelete) {
                        let indexOfItem = scope.listaB.findIndex(function retornaIndex(item) {
                            return angular.equals(item, scope.elementToDelete[0]);
                        })
                        if (indexOfItem != ELEMENT_NOT_FOUND) {
                            let element = scope.listaB.splice(indexOfItem, 1);
                            scope.listaA.push(element[0]);
                        }
                    }
                }
                scope.elementToAdd = "";
                scope.elementeToDelete = "";
            }

            scope.doMoveAllElement = function (directionListAToListB) {
                let arrayTemp = "";
                if (directionListAToListB) {
                    arrayTemp = scope.listaB.concat(scope.listaA);
                    scope.listaB = arrayTemp;
                    scope.listaA = [];
                } else {
                    arrayTemp = scope.listaA.concat(scope.listaB);
                    scope.listaA = arrayTemp;
                    scope.listaB = [];
                }
            }

        },
        templateUrl: csMultiSelectTemplate
    };
};

export default csMultiSelect;