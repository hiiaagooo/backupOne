'use strict';
import csMaskCpfCnpjDirective from './csMaskCpfCnpj.directive.js';

var m = angular.module('csMaskCpfCnpj', [])
    .directive('csMaskCpfCnpj', csMaskCpfCnpjDirective);

export default m;
