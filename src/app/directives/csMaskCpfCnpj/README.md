# cpfCnpj
## Descrição
Máscara para realizar a formatação do CPF ou CNPJ em PT-BR.

Opcionais:
* Validação dos dados
* Passar valor COM ou SEM máscara para o model

Exemplos:

Para CPF: *print_1.png*
![print de exemplo](/print_1.png)

Para CNPJ: *print_2.png*
![print de exemplo](/print_2.png)

## Como usar
* No HTML:

`<input ... mask-cpf-cnpj maxlength="18"/>`

| Resultado para  | Valor final    |
| --------------- | -------------- |
| view            | 123.456.789-12 |
| model           | 12345678912    |

Adicionando a tag `model-mask="true"` o valor enviado para o model também terá a máscara, veja:

`<input ... mask-cpf-cnpj model-mask="true" maxlength="18"/>`

| Resultado para  | Valor final    |
| --------------- | -------------- |
| view            | 123.456.789-12 |
| model           | 123.456.789-12 |

Para que a validação dos dados seja realizada, adicione a tag `validate-digit="true"`.


#### Parâmetros

| Parâmetro  | Descrição     | Valor | Obrigatório |
| ---------- |:-------------:| -----:| -----------:|
| `model-mask`   | Indica se o valor passado para o model deve conter os caracteres da máscara | true | Não |
| `validate-digit`   | Indica se deve ser realizado a validação dos dados | true | Não |