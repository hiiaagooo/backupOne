'use strict'
import csModalDirective from './csModal.directive'

export default angular.module('csModal', [])
    .directive('csModal', csModalDirective);