import csPaginatorNgTableDirective from './csPaginatorNgTable.directive'

export default angular.module('csPaginatorNgTable', [])
    .directive('csPaginatorNgTable', csPaginatorNgTableDirective)