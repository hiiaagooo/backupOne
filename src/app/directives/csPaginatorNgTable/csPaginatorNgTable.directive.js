/**
 * Created by fbferreira on 08/01/2018.
 */
/* Diretiva
 *      TODO documentacao
 *      TODO ajuste CSS primeira coluna quando Acao, fazer melhor tamanho quanto a largura da coluna
 *      TODO não apresentar mensagem de dados não encontrados ao iniciar a tabela
 * */
'use strict';
import csPaginatorNgTableTemplate from './csPaginatorNgTable.template.html';

function csPaginatorNgTable (NgTableParams) {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            funcao: "=",
            table: "="
        },
        link: function(scope, element, attrs, controllers) {

            scope.internalControl = scope.funcao || {};

            scope.internalControl.doNgTable = function (data, countList, countRowPerPage, showCountPage, showFilterColumns) {

                // check the optional parameters
                scope.countPerPage = countRowPerPage ? countRowPerPage : "30";
                scope.showCountPage = showCountPage ? false : true;
                scope.showFilterColumns = showFilterColumns ? true : false;
                scope.searchComplete = false;

                // set NgTableParams
                scope.tableParams = new NgTableParams({
                    page: 1,
                    count: scope.countPerPage
                }, {
                    counts: [],
                    getData: getList
                });

                function getList (params) {
                    scope.tableParams.total(countList);

                    scope.pagina = scope.tableParams._params.page;
                    let orderField = Object.keys(scope.tableParams._params.sorting) ? Object.keys(scope.tableParams._params.sorting) : "";
                    let orderDirection = Object.values(scope.tableParams._params.sorting) ? Object.values(scope.tableParams._params.sorting) : "";

                    var list = undefined;

                    return data(scope.pagina, scope.countPerPage, orderField, orderDirection)
                        .then(lista => {
                            console.log('directive: Array retornado da consulta ->', lista);

                            list = lista;
                            scope.listWithData = list.length > 0 ? true : false;
                            scope.searchComplete = true;

                            return list;
                        });
                }

                scope.selectedPageSizes = scope.tableParams.settings().counts;
                scope.availablePageSizes = [5, 10, 15, 20, 25, 30, 40, 50, 100];
                scope.changePage = changePage;
                scope.changePageSize = changePageSize;
                scope.changePageSizes = changePageSizes;

                updateTable();
            }

            // table control pages
            function changePage(nextPage){
                scope.tableParams.page(nextPage);
            }

            function changePageSize(newSize){
                scope.tableParams.count(newSize);
                scope.countPerPage = newSize;
            }

            function changePageSizes(newSizes){
                // ensure that the current page size is one of the options
                if (newSizes.indexOf(scope.tableParams.count()) === -1) {
                    newSizes.push(scope.tableParams.count());
                    newSizes.sort();
                }
                scope.tableParams.settings({counts: newSizes});
            }

            function updateTable() {
                scope.table = scope.tableParams;
            }
        },
        templateUrl: csPaginatorNgTableTemplate
    }
};

export default csPaginatorNgTable;