'use strict'
/* Diretiva para validar formularios
 *
 *   Mode de usar:
 *      HTML:
 *          adicionar no html do campo a tag "cs-validar", mais tag opcionais:
 *                      tag               :       descricao
 *              obrigatorio="true"        : campo deve possuir valor
 *              tipo="'tipoValidacao'"    : realiza a validacao do campo conforme o tipo informado, valores possiveis: 'data',
 *              limite-caracteres="n"     : 'n' valor numerico que representa a quantidade maxima de caracteres permitida
 *
 *          adicionar a os campos com a tag "cs-validar", dentro da tag: <form name="form"> campos </form>
 *
 *      Controller:
 *          adicionar na funcao que realiza o submit dos campos:
 *              $scope.$broadcast('cs-validar');
 *              if ($scope.form.$valid) {
 *                  // seu codigo para campos validados com sucesso
 *              }
 *
 *      Observacao: o valor do "name" da tag <form name="form"> é para fazer a referencia em "$scope.form.$valid"
 *
 *  Exemplos:
 *      <form name="form">
 *          <input type="text" ... cs-validar obrigatorio="true" />
 *          <input type="text" ... cs-validar tipo="'data'" />
 *          <input type="text" ... cs-validar limite-caracteres="200" />
 *          <input type="text" ... cs-validar obrigatorio="true" limite-caracteres="355" />
 *      </form>
 *
 *      vm.pesquisarNomes = function () {
 *          $scope.$broadcast('cs-validar');
 *          if ($scope.form.$valid) {
 *              // codigo para pesquisar os nomes
 *          }
 *      }
 *
 * */
function csValidar($compile) {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            model: '=ngModel',
            obrigatorio: '=',
            tipo: '=',
            quantidadeMinimaCaracteres: '='
        },
        link: function (scope, element, attrs, ctrl) {

            var campoInvalido = false;
            var campoPreenchido = false;
            var mensagemErro = "";

            // valida quando acionado a partir do controller
            scope.$on('cs-validar', function (event, formImpactado) {
                if (element[0].form.name == formImpactado) {
                    scope.validarCampo();
                    if (campoInvalido) {
                        var camposInvalidos = angular.element(document.querySelectorAll(".campoInvalidoFocus"));
                        var primeiroCampoInvalido = camposInvalidos[0];
                        primeiroCampoInvalido.focus();
                    }
                }
            });

            scope.validarCampo = function () {
                campoInvalido = false;
                scope.formatarCampo();
                scope.validarCampoPreenchido();

                if (campoPreenchido) {

                    switch (scope.tipo) {
                        case "data":
                            scope.validarCampoData();
                            break;
                        default:
                            break;
                    }

                    if (scope.quantidadeMinimaCaracteres > 0 && (!campoInvalido)) {
                        scope.validarCampoQuantidadeMinimaCaracteres();
                    }

                } else {
                    if (scope.obrigatorio) {
                        mensagemErro = "Campo obrigatório.";
                        campoInvalido = true;
                    }
                }

                if (campoInvalido) {
                    scope.formatarCampoInvalido();
                }
            }

            scope.validarIntervaloDeDatas = function () {

                mensagemErro = "Período inválido";

                var intervaloInvalido = false;
                if (scope.$parent.form.dataInicial.$viewValue && scope.$parent.form.dataFinal.$viewValue) {
                    // Modificar o nome do form para ficar dinamico
                    var dataInicial = new Date(scope.$parent.form.dataInicial.$viewValue.split("/").reverse().join("/"));
                    var dataFinal = new Date(scope.$parent.form.dataFinal.$viewValue.split("/").reverse().join("/"));

                    if (dataInicial > dataFinal) {
                        intervaloInvalido = true;
                    }
                    if (dataInicial < dataFinal) {
                        intervaloInvalido = false;
                    }
                }

                if (intervaloInvalido) {
                    campoInvalido = true;
                }
            }

            scope.validarCampoPreenchido = function () {
                switch (scope.model) {
                    case '':
                    case undefined:
                    case false:
                    case null:
                    case 'Selecione':
                    case 'Todos':
                        campoPreenchido = false;
                        break;
                    default:
                        campoPreenchido = true;
                        break;
                }
            }
            scope.validarCampoQuantidadeMinimaCaracteres = function () {
                if (scope.model.length < scope.quantidadeMinimaCaracteres) {
                    mensagemErro = "Campo deve conter no mínimo " + scope.quantidadeMinimaCaracteres + " caracteres.";
                    campoInvalido = true;
                }
            }
            scope.validarCampoData = function () {
                mensagemErro = "Data inválida";

                var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
                var dateInValid = false;

                var dataInformada = scope.model;

                if (!dataInformada) {
                    dateInValid = true;
                } else {
                    if (dataInformada.match(dateformat)) {

                        var opera1 = dataInformada.split('/');
                        var opera2 = dataInformada.split('-');
                        var lopera1 = opera1.length;
                        var lopera2 = opera2.length;

                        var pdate = 0;
                        if (lopera1 > 1) {
                            pdate = dataInformada.split('/');
                        } else if (lopera2 > 1) {
                            pdate = dataInformada.split('-');
                        }

                        var dd = parseInt(pdate[0]);
                        var mm = parseInt(pdate[1]);
                        var yy = parseInt(pdate[2]);

                        var listOfDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                        if (mm == 1 || mm > 2) {
                            if (dd > listOfDays[mm - 1]) {
                                dateInValid = true;
                            }
                        }

                        if (mm == 2) {
                            var lyear = false;
                            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                                lyear = true;
                            }
                            if ((lyear == false) && (dd >= 29)) {
                                dateInValid = true;
                            }
                            if ((lyear == true) && (dd > 29)) {
                                dateInValid = true;
                            }
                        }
                    } else {
                        dateInValid = true;
                    }
                }

                if (dateInValid) {
                    campoInvalido = true;
                } else {
                    campoInvalido = false;
                    scope.validarIntervaloDeDatas();
                }
            }

            scope.formatarCampo = function () {
                ctrl.$setValidity('valido', true)
                element.removeClass('campoInvalidoFocus');
                element.parent().removeClass('campoInvalido');
                element.next().remove();
            }
            scope.formatarCampoInvalido = function () {
                element.addClass('campoInvalidoFocus');
                element.parent().addClass('campoInvalido');

                ctrl.$setValidity('valido', false)

                var conteudoMensagemErro = angular.element('<span id="mensagemErro" name="mensagemErro">' + mensagemErro + '</span>');
                conteudoMensagemErro.insertAfter(element);
                $compile(conteudoMensagemErro)(scope);
            }
        }
    };
};

csValidar.$inject = ['$compile'];

export default csValidar;