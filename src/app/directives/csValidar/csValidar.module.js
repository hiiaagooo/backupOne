'use strict'
import csValidarDirective from './csValidar.directive'

export default angular.module('csValidar', [])
    .directive('csValidar', csValidarDirective);