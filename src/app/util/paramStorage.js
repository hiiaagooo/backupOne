'use strict';
function ParamStorage() {
    var storage = {
        local: window.localStorage,
        session: window.sessionStorage
    };

    this.get = function (key) {
        return storage.local.getItem(key);
    }

    this.set = function (key, value) {
        storage.local.setItem(key, value);
    }
}


ParamStorage.$inject = [];

export default angular.module('ParamStorage', [])
    .service('ParamStorage', ParamStorage);