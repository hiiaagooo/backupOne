'use strict';
function IdleConfig(IdleProvider, KeepaliveProvider, TitleProvider) {
    // configure Idle settings
    IdleProvider.idle(parseInt(process.env.IDLE_TIME)); // in seconds
    IdleProvider.timeout(parseInt(process.env.IDLE_TIMEOUT)); // in seconds
    KeepaliveProvider.interval(parseInt(process.env.IDLE_INTERVAL)); // in seconds

    TitleProvider.enabled(false);
}

IdleConfig.$inject = ['IdleProvider', 'KeepaliveProvider', 'TitleProvider'];

angular.module(process.env.APP_NAME).config(IdleConfig);