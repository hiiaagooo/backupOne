'use strict';
function MomentPickerConfig(momentPickerProvider) {
    momentPickerProvider.options({
        /* Picker properties */
        locale:        'pt-br',
        format:        'L',
        minView:       'decade',
        maxView:       'minute',
        startView:     'year',
        autoclose:     true,
        today:         true,
        keyboard:      true,

        /* Extra: Views properties */
        leftArrow:     '&larr;',
        rightArrow:    '&rarr;',
        yearsFormat:   'YYYY',
        monthsFormat:  'MMM',
        daysFormat:    'D',
        hoursFormat:   'HH:[00]',
        minutesFormat: moment.localeData().longDateFormat('LT').replace(/[aA]/, ''),
        secondsFormat: 'ss',
        minutesStep:   5,
        secondsStep:   1
    });

    moment.locale('pt-BR');
}

MomentPickerConfig.$inject = ['momentPickerProvider'];

angular.module(process.env.APP_NAME).config(MomentPickerConfig);