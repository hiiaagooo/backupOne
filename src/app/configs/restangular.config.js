'use strict';

import csMessageService from "../directives/csMessage/csMessage.service";

function RestangularConfig(RestangularProvider) {
    RestangularProvider.setBaseUrl(process.env.REST_BASE);
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
    RestangularProvider.setDefaultHttpFields({
        withCredentials: true
    });

    RestangularProvider.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
        if (operation === 'getList') {
            var newResponse = [];
            if (data.dados) {
                newResponse = data.dados;
            }
            return newResponse;
        }
        return data;
    });
}

function RestangularErrorInterceptor(csMessageService, Restangular) {
    Restangular.setErrorInterceptor(function (response, deferred, responseHandler) {
        if (response.data) {
            csMessageService.setMessage(response.data.mensagens[0], csMessageService.ERROR);
        } else {
            csMessageService.setMessage('Um erro inesperado ocorreu.', csMessageService.ERROR);
        }
    });
}

RestangularConfig.$inject = ['RestangularProvider'];

RestangularErrorInterceptor.$inject = ['csMessageService', 'Restangular']

angular.module(process.env.APP_NAME)
    .config(RestangularConfig)
    .run(RestangularErrorInterceptor);