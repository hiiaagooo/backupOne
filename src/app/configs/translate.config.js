'use strict';
function TranslateConfig($translateProvider) {
    $translateProvider.translations('en', require('../i18n/en.json'));
    $translateProvider.translations('pt_BR', require('../i18n/pt_BR.json'));
    $translateProvider.preferredLanguage('pt_BR');
    $translateProvider.useSanitizeValueStrategy(null);

}

TranslateConfig.$inject = ['$translateProvider'];

angular.module(process.env.APP_NAME).config(TranslateConfig);