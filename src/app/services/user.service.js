function UserService (Restangular, $sessionStorage) {

    Object.assign(this, Restangular.service('private/usuario'));

    this.setUser = () => {
        return this.one('getUser').get();

    };

    this.getUser = () => {
        return  $sessionStorage.user
    };

    this.validateTicket = (service, ticket) => {
        let req = this.one('validate-token').post('', {
            service,
            ticket
        });
        return req;
    };

    this.hasUserAuthenticated = () => {
        return !!$sessionStorage.user;
    };

    this.getServicosByIdentificacaoAndNrCartao = (dados) => {
        let req = this.one('lista-servicos-cartao').post('',dados);
        return req
    };
};

UserService.$inject = ['Restangular', '$sessionStorage'];

export default angular.module('UserService', [])
    .service('UserService', UserService);