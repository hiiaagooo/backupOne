'use strict';

import CryptService from './crypt.service';

function AuthService($rootScope, $q, $state, Restangular, grant, CryptService, blockUI) {

    var vm = this;

    var storage = {
        local: window.localStorage,
        session: window.sessionStorage
    };

    vm.resource = Restangular.one('oauth');

    vm.getUser = function () {
        return JSON.parse(CryptService.decode(storage.session.getItem(process.env.APP_NAME + '_user')));
    };

    vm.getToken = function () {
        if (storage.session.getItem(process.env.APP_NAME + '_token'))
            return JSON.parse(CryptService.decode(storage.session.getItem(process.env.APP_NAME + '_token')));
        return false
    };

    vm.lockUser = function () {
        var user = JSON.parse(CryptService.decode(storage.session.getItem(process.env.APP_NAME + '_user')));
        if (!user.locked) {
            user.locked = true;
            storage.session.setItem(process.env.APP_NAME + '_user', CryptService.encode(JSON.stringify(user)));
        }
    };

    vm.unlockUser = function (credentials) {
        var user = JSON.parse(CryptService.decode(storage.session.getItem(process.env.APP_NAME + '_user')));

        var deferred = $q.defer();

        var RestWithHeader = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setDefaultRequestParams(["get", "post", "put", "delete"], {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: process.env.GRANT_TYPE,
                scope: "read,write,trust",
                username: user.username,
                password: credentials.password,
                sistema: process.env.ID_SISTEMA_CSA,
            });
        });

        blockUI.start();

        RestWithHeader.one('oauth').post('token', {/*Corpo do body aqui*/})
            .then(function (res) {
                delete user.locked;
                res.expires = moment().add(res.expires_in, 'seconds');
                storage.session.setItem(process.env.APP_NAME + '_user', CryptService.encode(JSON.stringify(user)));
                storage.session.setItem(process.env.APP_NAME + '_token', CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
                if (storage.local.getItem(process.env.APP_NAME + '_token_' + user.username)) {
                    storage.local.setItem(process.env.APP_NAME + '_user_' + user.username, CryptService.encode(JSON.stringify(user)));
                    storage.local.setItem(process.env.APP_NAME + '_token_' + user.username, CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
                }

                blockUI.stop();

                $state.go(!!$rootScope.previousState && $rootScope.previousState.name != 'auth.login' && $rootScope.previousState.name || 'home.main');

                deferred.resolve({});
            }, function (error) {
                deferred.reject(error);
            });

        return deferred.promise;
    };

    vm.authenticate = function (credentials, remember) {

        var deferred = $q.defer();
        // Workaround para mandar os dados para o endpoint
        // TODO Transferir essa configuração para o config

        var RestWithHeader = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setDefaultRequestParams(["get", "post", "put", "delete"], {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: process.env.GRANT_TYPE,
                scope: "read,write,trust",
                username: credentials.username,
                password: credentials.password,
                sistema: process.env.ID_SISTEMA_CSA,
            });
        });

        blockUI.start();

        // TODO passar username e password no corpo do post
        RestWithHeader.one('oauth').post('token', {/*Corpo do body aqui*/}).then(function (res) {
            delete credentials.password;
            res.expires = moment().add(res.expires_in, 'seconds');
            storage.session.setItem(process.env.APP_NAME + '_user', CryptService.encode(JSON.stringify(credentials)));
            storage.session.setItem(process.env.APP_NAME + '_token', CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
            if (remember) {
                storage.local.setItem(process.env.APP_NAME + '_user_' + credentials.username, CryptService.encode(JSON.stringify(credentials)));
                storage.local.setItem(process.env.APP_NAME + '_token_' + credentials.username, CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
            }

            blockUI.stop();

            $state.go(!!$rootScope.previousState && $rootScope.previousState.name != 'auth.login' && $rootScope.previousState.name || 'home.main');
            deferred.resolve({});
        }, function (error) {
            blockUI.stop();
            deferred.reject(error);
        });

        return deferred.promise;
    };

    vm.refreshToken = function () {
        var token = vm.getToken();
        var user = vm.getUser();

        var RestWithHeader = Restangular.withConfig(function (RestangularConfigurer) {
            RestangularConfigurer.setDefaultHeaders({"Authorization": "Bearer " + token.access_token});
            RestangularConfigurer.setDefaultRequestParams(["get", "post", "put", "delete"], {
                client_id: process.env.CLIENT_ID,
                client_secret: process.env.CLIENT_SECRET,
                grant_type: "refresh_token",
                refresh_token: token.refresh_token,
                scope: "read,write,trust",
            });
        });

        RestWithHeader.one('oauth').one('token').post()
            .then(function (res) {
                res.expires = moment().add(res.expires_in, 'seconds');
                storage.session.setItem(process.env.APP_NAME + '_token', CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
                if (storage.local.getItem(process.env.APP_NAME + '_token_' + user.username)) {
                    storage.local.setItem(process.env.APP_NAME + '_token_' + user.username, CryptService.encode(JSON.stringify(Restangular.stripRestangular(res))));
                }

            }, function (error) {
                console.log(error);
                vm.logout();
            });
    };

    vm.logout = function () {

        blockUI.start();

        Restangular.one('api/logout').get()
            .then(function () {
                storage.session.removeItem(process.env.APP_NAME + '_user');
                storage.session.removeItem(process.env.APP_NAME + '_token');

                blockUI.stop();

                $state.go('auth.login');
            }, function (error) {

                blockUI.stop();
                // if(error.data.error === "invalid_token"){
                storage.session.removeItem(process.env.APP_NAME + '_user');
                storage.session.removeItem(process.env.APP_NAME + '_token');
                $state.go('auth.login');
                // }
            });
    }

    /*
     * Regras do grant
     * */

    vm.initGrant = function () {
        grant.addTest('isAuthenticated', function () {
            return vm.isAuthenticated();
        });
    };

    vm.isAuthenticated = function () {
        if (storage.session.getItem(process.env.APP_NAME + '_user'))
            return true;
        return false;
    };

    vm.isLocked = function () {
        var user = JSON.parse(CryptService.decode(storage.session.getItem(process.env.APP_NAME + '_user')));
        if (user && user.locked)
            return true;
        return false;
    };

}

AuthService.$inject = ['$rootScope', '$q', '$state', 'Restangular', 'grant', 'CryptService', 'blockUI'];

export default angular.module('services.AuthService', [
    CryptService.name
])
    .service('AuthService', AuthService);