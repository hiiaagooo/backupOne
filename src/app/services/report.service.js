'use strict';

function ReportService(Restangular) {
    var vm = this;

    vm.service = Restangular.withConfig(function(configurer){
        configurer.setBaseUrl(process.env.REST_BASE + '/cadastros');
    }).service('cadastroReport');

    return vm.service;
}

ReportService.$inject = ['Restangular'];

export default angular.module('services.ReportService', [])
    .service('ReportService', ReportService);