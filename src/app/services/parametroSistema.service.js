'use strict';

function ParametroSistemaService(Restangular) {

    const service = Restangular.withConfig(function(configurer){
        configurer.setBaseUrl(process.env.REST_BASE);
    }).service('private/parametro-sistema');

    function consultar(params) {
        return service.getList({descricao: params});
    }

    function salvar(object) {
        return service.one().post(null,object)
    }

    function editar(id, object) {
        return service.one(id).customPUT(object)
    }

    function excluir(id) {
        return service.one(id).remove()
    }

    function buscar(id) {
        return service.one(id).get();
    }

    return {
        consultar,
        salvar,
        editar,
        buscar,
        excluir
    };
}

ParametroSistemaService.$inject = ['Restangular'];

export default angular.module('ParametroSistemaService', [])
    .service('ParametroSistemaService', ParametroSistemaService);
