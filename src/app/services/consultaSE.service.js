'use strict';

function ConsultaSEService(Restangular) {

    const service = Restangular.withConfig(function(configurer){
        configurer.setDefaultHeaders({'Content-Type': undefined});
        configurer.setBaseUrl(process.env.REST_BASE);
    }).service('private/superitendencia-estadual');


    //Obter SE's cadastradas
    function listarSE() {
        return service.one('listar-todas-se').getList();
    }

    // Listar faixas cadastradas
    function listarSESegmentadoPorUF(params) {
        return service.one('listar-segmentado-por-uf').post(params);
    }

    return {
        listarSE,
        listarSESegmentadoPorUF,
    };
}

ConsultaSEService.$inject = ['Restangular'];

export default angular.module('ConsultaSEService', [])
    .service('ConsultaSEService', ConsultaSEService);
