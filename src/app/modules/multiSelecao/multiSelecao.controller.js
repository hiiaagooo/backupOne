'use strict';

function MultiSelecaoController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    $http) {

    var vm = this;

    vm.listaA = [
        {
            id: 1,
            nome: "Felipe"
        },
        {
            id: 2,
            nome: "José"
        },
        {
            id: 3,
            nome: "Natalia"
        },
        {
            id: 4,
            nome: "João"
        },
        {
            id: 5,
            nome: "Maira"
        }
    ];

    vm.listaB = [];

}

MultiSelecaoController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    '$http'];
export default MultiSelecaoController;
