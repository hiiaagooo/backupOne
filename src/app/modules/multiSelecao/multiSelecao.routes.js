'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/multiSelecao.html';

function MultiSelecaoRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('multi-selecao', {
            url: '/multi-selecao',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Multi-Seleção'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./multiSelecao.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('multi-selecao.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'multi-selecao',
            controller: 'MultiSelecaoController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('multi-selecao.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'multi-selecao',
            controller: 'MultiSelecaoController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

MultiSelecaoRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('multi-selecao.routes', [])
    .config(MultiSelecaoRoutes);