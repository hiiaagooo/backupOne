'use strict';
import bootstrap from './multiSelecao.run';

import MultiSelecaoController from './multiSelecao.controller';

import ParamStorage from '../../util/paramStorage';

// import MultiSelectDirective from '../../directives/multiSelect/multiSelect.directive';

/*
 * Start the module with dependencies
 * */
let module = angular.module('multi-selecao', [
    ParamStorage.name,

    // MultiSelectDirective.name
]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('MultiSelecaoController', MultiSelecaoController);

export default module;
