'use strict';
import bootstrap from './modal.run';

import ModalController from './modal.controller';

import ParamStorage from '../../util/paramStorage';

// import ModalDirective from '../../directives/modal/modal.module';

/*
 * Start the module with dependencies
 * */
let module = angular.module('modal', [
    ParamStorage.name,

    // ModalDirective.name
]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('ModalController', ModalController);

export default module;
