'use strict';

function ModalController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    $http) {

    var vm = this;

    vm.usuarioSelecionado = {};
    vm.usuarios = [
        {
            id: 1,
            nome: "Felipe"
        },
        {
            id: 2,
            nome: "Gabi"
        },
        {
            id: 3,
            nome: "João"
        },
        {
            id: 4,
            nome: "Maria"
        },
    ];

    vm.salvar = function () {
        vm.show = !vm.show;

        // sua implementacao
    }

}

ModalController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    '$http'];
export default ModalController;
