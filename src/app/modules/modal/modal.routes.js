'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/modal.html';

function ModalRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('modal', {
            url: '/modal',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Modal'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./modal.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('modal.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'modal',
            controller: 'ModalController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('modal.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'modal',
            controller: 'ModalController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

ModalRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('modal.routes', [])
    .config(ModalRoutes);