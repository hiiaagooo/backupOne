'use strict';
function PaginadorModel() {

    var vm = this;

    vm.filtros = {
        descricao: "",
    };

    // reset os valores da pagina ao ser acessado pelo menu principal
    vm.reset = function(){
        vm.filtros = {
            descricao: "",
        };
    }
}

PaginadorModel.$inject = [];

export default angular.module('PaginadorModel', [
]).service('PaginadorModel', PaginadorModel);