'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/paginador.html';

function PaginadorRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('paginador', {
            url: '/paginador',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Paginador'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./paginador.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('paginador.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'paginador',
            controller: 'PaginadorController as vm',
            onEnter: ['PaginadorModel', function(PaginadorModel){
                PaginadorModel.reset();
            }],
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('paginador.voltar', {
            url: '/',
            ncyBreadcrumb: {
                label: 'Paginador'
            },
            templateUrl: homeView,
            parent: 'paginador',
            controller: 'PaginadorController as vm',
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

PaginadorRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('paginador.routes', [])
    .config(PaginadorRoutes);