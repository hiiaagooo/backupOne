'use strict';
import bootstrap from './paginador.run';

import PaginadorController from './paginador.controller';

import PaginadorModel from './paginador.model';

import ParamStorage from '../../util/paramStorage';

/*
 * Start the module with dependencies
 * */
let module = angular.module('paginador', [
    PaginadorModel.name,

    ParamStorage.name
]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('PaginadorController', PaginadorController);

export default module;
