'use strict';

function PaginadorController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    PaginadorModel,
    ParametroSistemaService,
    $http) {

    var vm = this;
    vm.model = PaginadorModel;
    vm.pesquisaRealizada = false;

    vm.ngTable = {};

    vm.ngTableUpdate = function (item) {
        console.log("Update");
        console.log(item);
    };

    vm.ngTableDetail = function (item) {
        console.log("Detail");
        console.log(item);
    };

    vm.ngTableRemove = function (item) {
        console.log("Remove");
        console.log(item);
    };

    vm.ngTableMore = function (item) {
        console.log("More");
        console.log(item);
    };

    // funcao responsavel por realizar a pesquisa para apresentar a tabela com paginatorNgTable
    vm.search = function () {
        vm.pesquisaRealizada = true;

        // funcoes habilitadas
        vm.functionUpdateAble = true;
        vm.functionDetailAble = false;
        vm.functionRemoveAble = true;
        vm.functionMoreAble = false;

        // parametros para criar a tabela
        let habilitarSelectQuantidadePorPagina = true;
        let habilitarFiltrosColunas = false;
        vm.quantidadeItensPorPagina = "5";
        vm.quantidadeTotalRegistros = "100"; // passar o tamanho total da lista (count() / .length)

        vm.ngTable.doNgTable(vm.pesquisarParametroSistema, vm.quantidadeTotalRegistros);
        // vm.ngTable.doNgTable(vm.pesquisarParametroSistema, vm.quantidadeTotalRegistros, vm.quantidadeItensPorPagina, habilitarSelectQuantidadePorPagina, habilitarFiltrosColunas);
    }

    vm.pesquisarParametroSistema = function (pagina, quantidadeItensPorPagina, campoOrdenacao, direcaoOrdenacao) {

        console.log("############ Pesquisar Parametro Sistema");
        console.log("Pagina: "+pagina);
        console.log("Quantidade de itens por página: "+quantidadeItensPorPagina);
        console.log("Ordenação_campo: "+campoOrdenacao);
        console.log("Ordenação_direção: "+direcaoOrdenacao);

        var requisicaoDB = new Promise((resolve,reject) =>{
            ParametroSistemaService
                .consultar(vm.model.filtros.descricao)
                .then(function (res) {
                    // resolve([{nome: "Felipe", valor: 25, descricao: "Fazer diretiva para paginação de array retornado de uma requisação do banco de dados", tipoParametro: "Alto"}]);
                    resolve(res);
                }, function () {
                    // resolve([]);

                    // teste - como nao tem conexao com o back erro nos testes TODO apagar if else abaixo
                    if (pagina == 1) {
                        resolve([
                            {nome: "Felipe", valor: 25, descricao: "Fazer diretiva para paginação de array retornado de uma requisação do banco de dados", tipoParametro: "Alto"},
                            {nome: "Maira", valor: 12, descricao: "Somente mais um usuário para ser apresentado nessa lista", tipoParametro: "Médio"}
                        ]);
                    } else {
                        resolve([
                            {nome: "Ana", valor: 53, descricao: "Outro usuário retornado do banco de dados", tipoParametro: "Baixo"},
                            {nome: "José", valor: 31, descricao: "Mais um usuário para ser apresentado nessa lista", tipoParametro: "Médio"},
                            {nome: "João", valor: 42, descricao: "Dados de usuário para ser exibido aqui", tipoParametro: "Alto"},
                            {nome: "Gabi", valor: 7, descricao: "Apresentar informações de outro user", tipoParametro: "Médio"}
                        ]);
                    }
                });
        })

        return requisicaoDB;
    };

}

PaginadorController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    'PaginadorModel',
    'ParametroSistemaService',
    '$http'];
export default PaginadorController;
