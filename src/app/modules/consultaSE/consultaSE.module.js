'use strict';
import bootstrap from './consultaSE.run';

import ConsultaSEController from './consultaSE.controller';

import ConsultaSEModel from './consultaSE.model';

import ParamStorage from '../../util/paramStorage';

import MessageContants from './../../constants/message.constant'
import MessageDirective from '../../directives/csMessage/csMessage.module';
import ModalDirective from '../../directives/csModal/csModal.module';
import ValidarDirective from '../../directives/csValidar/csValidar.module';

import ConsultaSEService from '../../services/consultaSE.service';

/*
 * Start the module with dependencies
 * */
let module = angular.module('ConsultaSE', [
    ConsultaSEModel.name,

    ParamStorage.name,

    MessageContants.name,

    MessageDirective.name,
    ValidarDirective.name,
    ModalDirective.name,

    ConsultaSEService.name

]);

/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('ConsultaSEController', ConsultaSEController);

export default module;
