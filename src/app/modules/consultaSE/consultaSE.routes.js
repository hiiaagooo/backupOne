'use strict';

import homeView from './views/consultaSEPesquisar.html';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

function ConsultaSERoutes($urlRouterProvider, $stateProvider, $breadcrumbProvider) {
    $urlRouterProvider.otherwise('/');

    $breadcrumbProvider.setOptions({
        prefixStateName: 'home',
        includeAbstract: true
    });


    $stateProvider
        .state('consulta-se', {
            url: '/consulta-se',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'ConsultaSe'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./consultaSE.module').default;
                            $ocLazyLoad.load({
                                name: module.name
                            });
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('consulta-se.pesquisar', {
            url: '/pesquisar',
            templateUrl: homeView,
            parent: 'consulta-se',
            controller: 'ConsultaSEController as vm',
            ncyBreadcrumb: {
                label: 'Pesquisar'
            },
            onEnter: ['ConsultaSEModel', function (ConsultaSEModel) {
                ConsultaSEModel.reset();
            }],
            resolve: {
                user: function (grant) {
                    return grant.only([{
                            test: 'isAuthenticated',
                            state: 'auth.login'
                        },
                        {
                            test: 'isNotLocked',
                            state: 'auth.lock'
                        },
                    ]);
                }
            }
        })
}

ConsultaSERoutes.$inject = ['$urlRouterProvider', '$stateProvider', '$breadcrumbProvider'];

export default angular
    .module('consultaSE.routes', [])
    .config(ConsultaSERoutes);