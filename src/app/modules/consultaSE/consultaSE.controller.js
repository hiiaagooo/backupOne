'use strict';

function ConsultaSEController($scope, $state,
                              ConsultaSEService,
                              ConsultaSEModel, csMessageService, MessageConstants) {

    var vm = this;
    vm.model = ConsultaSEModel;
    vm.consultarSE = false;


    // Listar SE's (combo)
    function listarSE () {

        ConsultaSEService
            .listarSE()
            .then(function (res) {
                vm.model.filtros.siglaSE = res;
                vm.model.dadoSelecionado.mcuSelecionado = //?
                vm.model.dadoSelecionado.seSelecionada =  //?

                console.log('teste', vm.model.dadoSelecionado.seSelecionada)
            });
    };


    // Listar Segmentados por UF
    vm.listarSESegmentadoPorUF = function () {

        var params = {
            mcu: vm.model.dadoSelecionado.mcuSelecionado, //codigo
            nome: vm.model.dadoSelecionado.seSelecionada, //sigla
        };

        ConsultaSEService
            .listarSESegmentadoPorUF($state.params)
            .then(function (res) {
                vm.model.listaSegmentadoPorUF = res;
            }, function () {
        });
    };


    function init() {
        vm.model.reset();
        listarSE();
    };

    init();
}

ConsultaSEController.$inject = ['$scope', '$state',
    'ConsultaSEService', 'ConsultaSEModel',
    'csMessageService', 'MessageConstants'];


export default ConsultaSEController;

