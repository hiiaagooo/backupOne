'use strict';
function ConsultaSEModel() {

    var vm = this;

    // armazena as listas trazidas pelo backend.
    vm.listaSegmentadoPorUF = [];

    // armazena a SE/MCU selecionados.
    vm.dadoSelecionado = {
        seSelecionada: "",
        mcuSelecionado: "",
    };

    // armazena o valor pesquisado pelo usuário - tela 1
    vm.filtros = {
        siglaSE: "",
        nomeSE: "",
        };

    // reset os valores da pagina ao ser acessado pelo menu principal
    vm.reset = function(){
        vm.listaSE = [];
        vm.filtros = {
            siglaSE: "",
            nomeSE: "",
        };
    }
}

ConsultaSEModel.$inject = [];

export default angular.module('ConsultaSEModel', [
]).service('ConsultaSEModel', ConsultaSEModel);
