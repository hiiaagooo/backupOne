'use strict';

function MascarasController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    $http) {

    var vm = this;

    vm.cpfCnpj;
    vm.dataBr;

}

MascarasController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    '$http'];
export default MascarasController;
