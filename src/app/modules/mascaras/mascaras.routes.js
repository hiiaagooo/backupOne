'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/mascaras.html';

function MascarasRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('mascaras', {
            url: '/mascaras',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Máscaras'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./mascaras.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('mascaras.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'mascaras',
            controller: 'MascarasController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('mascaras.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'mascaras',
            controller: 'MascarasController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

MascarasRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('mascaras.routes', [])
    .config(MascarasRoutes);