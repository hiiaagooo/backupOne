'use strict';
import bootstrap from './mascaras.run';

import MascarasController from './mascaras.controller';

import ParamStorage from '../../util/paramStorage';

import CsMascaraDataBr from '../../directives/csMaskDataBr/csMaskDataBr.module';

/*
 * Start the module with dependencies
 * */
let module = angular.module('modal', [
    ParamStorage.name,

    CsMascaraDataBr.name
]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('MascarasController', MascarasController);

export default module;
