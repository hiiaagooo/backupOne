'use strict';

function ModalDialogController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    $http,
    MessageConstants,
    csMessageService) {

    var vm = this;

    vm.tituloExcluir = "Excluir";
    vm.tituloAlterar = "Alterar";

    vm.textoExcluirConfirmar = MessageConstants.EXCLUSAO_CONFIRMA;
    vm.textoAlterarConfirmar = MessageConstants.ALTERACAO_CONFIRMA;

    vm.excluirItem = function () {
        console.log("Excluir item!");
        csMessageService.setMessage(MessageConstants.ALTERACAO_SUCESSO, csMessageService.SUCCESS);
    };

    vm.alterarItem = function () {
        console.log("Alterar item!");
    };

}

ModalDialogController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    '$http',
    'MessageConstants',
    'csMessageService'];
export default ModalDialogController;
