'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/modalDialog.html';

function ModalDialogRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('modal-dialog', {
            url: '/modal-dialog',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Modal-Dialog'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./modalDialog.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('modal-dialog.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'modal-dialog',
            controller: 'ModalDialogController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('modal-dialog.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'modal-dialog',
            controller: 'ModalDialogController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

ModalDialogRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('modal-dialog.routes', [])
    .config(ModalDialogRoutes);