'use strict';
import bootstrap from './modalDialog.run';

import ModalDialogController from './modalDialog.controller';

/*
 * Start the module with dependencies
 * */
let module = angular.module('modal', [

]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('ModalDialogController', ModalDialogController);

export default module;
