'use strict';

import homeView from './views/home.html';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

function defaultRouting($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home', {
            url: '',
            templateUrl: layoutTemplate,
            abstract: true,
            ncyBreadcrumb: {
                label: 'Home'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./default.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('home.main', {
            url: '/',
            templateUrl: homeView,
            controller: 'default.MainController as vm',
            ncyBreadcrumb: {
                skip: true
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

defaultRouting.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
        .module('default.routes', [])
        .config(defaultRouting);