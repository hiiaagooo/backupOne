'use strict';

import bootstrap from './default.run';

import MainController from './main.controller';

/*
 * Start the module with dependencies
 * */
let module = angular.module('default', [
    // Aqui vem todas as dependencias do modulo
    //Ex: require('../../models/example.models').default.name
]);

/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('default.MainController', MainController);

export default module;


