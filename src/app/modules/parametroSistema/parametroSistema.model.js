'use strict';
function ParametroSistemaModel() {

    var vm = this;

    // armazena a lista referente a pesquisa - tela 1
    vm.listaParametroSistema = [];

    // indica se a pesquisa foi realizada
    vm.pesquisaRealizada = false;

    // indica se a edição foi salva
    vm.edicaoRealizada = false;

    // armazena o valor pesquisado pelo usuário - tela 1
    vm.filtros = {
        descricao: "",
    };

    // valida os campos obrigatorios
    vm.validator = false;

    // armazena o item a ser editado
    vm.dadoItemEdicao = {};

    // reset os valores da pagina ao ser acessado pelo menu principal
    vm.reset = function(){
        vm.listaParametroSistema = [];
        vm.filtros = {
            descricao: "",
        };
        vm.pesquisaRealizada = false;
        vm.atualizacaoRealizada = false;
    }
}

ParametroSistemaModel.$inject = [];

export default angular.module('ParametroSistemaModel', [
]).service('ParametroSistemaModel', ParametroSistemaModel);