'use strict';
import bootstrap from './parametroSistema.run';

import ParametroSistemaController from './parametroSistema.controller';

import ParametroSistemaModel from './parametroSistema.model';

import ParamStorage from '../../util/paramStorage';

import MessageContants from './../../constants/message.constant'
import CsValidarDirective from '../../directives/csValidar/csValidar.module';

import ParametroSistemaService from '../../services/parametroSistema.service';
import ReportService from '../../services/report.service';

/*
 * Start the module with dependencies
 * */
let module = angular.module('parametroSistema', [
    ParametroSistemaModel.name,

    ParamStorage.name,

    MessageContants.name,

    CsValidarDirective.name,

    ParametroSistemaService.name,
    ReportService.name

]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('ParametroSistemaController', ParametroSistemaController);

export default module;
