'use strict';

import homeView from './views/parametroSistemaPesquisar.html';
import editarView from './views/parametroSistemaCadastrar.html';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

function ParametroSistemaRoutes($urlRouterProvider, $stateProvider, $breadcrumbProvider) {
    $urlRouterProvider.otherwise('/');

    $breadcrumbProvider.setOptions({
        prefixStateName: 'home',
        includeAbstract: true
    });


    $stateProvider
        .state('parametro-sistema', {
            url: '/parametro-sistema',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Parâmetro'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./parametroSistema.module').default;
                            $ocLazyLoad.load({
                                name: module.name
                            });
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('parametro-sistema.pesquisar', {
            url: '/pesquisar',
            templateUrl: homeView,
            parent: 'parametro-sistema',
            controller: 'ParametroSistemaController as vm',
            ncyBreadcrumb: {
                label: 'Pesquisar'
            },
            onEnter: ['ParametroSistemaModel', function (ParametroSistemaModel) {
                ParametroSistemaModel.reset();
            }],
            resolve: {
                user: function (grant) {
                    return grant.only([{
                            test: 'isAuthenticated',
                            state: 'auth.login'
                        },
                        {
                            test: 'isNotLocked',
                            state: 'auth.lock'
                        },
                    ]);
                }
            }
        })
        .state('parametro-sistema.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'parametro-sistema',
            controller: 'ParametroSistemaController as vm',
            ncyBreadcrumb: {
                label: 'Pesquisar'
            },
            resolve: {
                user: function (grant) {
                    return grant.only([{
                            test: 'isAuthenticated',
                            state: 'auth.login'
                        },
                        {
                            test: 'isNotLocked',
                            state: 'auth.lock'
                        },
                    ]);
                }
            }
        })
        .state('parametro-sistema.cadastrar', {
            url: '/cadastrar',
            templateUrl: editarView,
            parent: 'parametro-sistema',
            controller: 'ParametroSistemaController as vm',
            ncyBreadcrumb: {
                label: 'Cadastrar',
            },
            resolve: {
                user: function (grant) {
                    return grant.only([{
                            test: 'isAuthenticated',
                            state: 'auth.login'
                        },
                        {
                            test: 'isNotLocked',
                            state: 'auth.lock'
                        },
                    ]);
                }
            }
        })
        .state('parametro-sistema.editar', {
            url: '/editar/{id}',
            templateUrl: editarView,
            parent: 'parametro-sistema',
            controller: 'ParametroSistemaController as vm',
            ncyBreadcrumb: {
                label: 'Editar'
            },
            resolve: {
                user: function (grant) {
                    return grant.only([{
                            test: 'isAuthenticated',
                            state: 'auth.login'
                        },
                        {
                            test: 'isNotLocked',
                            state: 'auth.lock'
                        },
                    ]);
                }
            }
        });
}

ParametroSistemaRoutes.$inject = ['$urlRouterProvider', '$stateProvider', '$breadcrumbProvider'];

export default angular
    .module('parametroSistema.routes', [])
    .config(ParametroSistemaRoutes);