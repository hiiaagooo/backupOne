'use strict';

function ParametroSistemaController($scope, $rootScope, Restangular, $state,
                                    ParametroSistemaService, ReportService,
                                    ParametroSistemaModel, csMessageService, $http, MessageConstants) {
    var vm = this;
    vm.model = ParametroSistemaModel;
    vm.pesquisaRealizada = false;

    vm.tituloExcluir = "Excluir";
    vm.textoExcluirConfirmar = MessageConstants.EXCLUSAO_CONFIRMA;


    //  -------------------------- paginatorNgTable --------------------------
    vm.ngTable = {};

    vm.ngTableUpdate = function (item) {
        console.log("Update");
        console.log(item);
    };

    vm.ngTableDetail = function (item) {
        console.log("Detail");
        console.log(item);
    };

    // vm.ngTableRemove = function (item) {
    //     console.log("Remove");
    //     console.log(item);
    // };

    vm.ngTableMore = function (item) {
        console.log("More");
        console.log(item);
    };

    // funcao responsavel por realizar a pesquisa para apresentar a tabela com paginatorNgTable
    vm.search = function () {
        vm.pesquisaRealizada = true;

        // funcoes habilitadas
        vm.functionUpdateAble = true;
        vm.functionDetailAble = false;
        vm.functionRemoveAble = true;
        vm.functionMoreAble = false;

        // parametros para criar a tabela
        let habilitarSelectQuantidadePorPagina = true;
        let habilitarFiltrosColunas = false;
        vm.quantidadeItensPorPagina = "5";
        vm.quantidadeTotalRegistros = "100"; // passar o tamanho total da lista (count() / .length)

        vm.ngTable.doNgTable(vm.pesquisarParametroSistema, vm.quantidadeTotalRegistros, vm.quantidadeItensPorPagina, habilitarSelectQuantidadePorPagina, habilitarFiltrosColunas);
    }

    vm.pesquisarParametroSistema = function (pagina, quantidadeItensPorPagina, campoOrdenacao, direcaoOrdenacao) {

        console.log("############ Pesquisar Parametro Sistema");
        console.log("Pagina: "+pagina);
        console.log("Quantidade de itens por página: "+quantidadeItensPorPagina);
        console.log("Ordenação_campo: "+campoOrdenacao);
        console.log("Ordenação_direção: "+direcaoOrdenacao);

        var requisicaoDB = new Promise((resolve,reject) =>{
            ParametroSistemaService
                .consultar(vm.model.filtros.descricao)
                .then(function (res) {
                    // resolve([{nome: "Felipe", valor: 25, descricao: "Fazer diretiva para paginação de array retornado de uma requisação do banco de dados", tipoParametro: "Alto"}]);
                    resolve(res);
                }, function () {
                    // resolve([]);

                    // teste - como nao tem conexao com o back erro nos testes TODO apagar if else abaixo
                    if (pagina == 1) {
                        resolve([
                            {id: 1, nome: "Felipe", valor: 25, descricao: "Fazer diretiva para paginação de array retornado de uma requisação do banco de dados", tipoParametro: "Alto"},
                            {id: 2, nome: "Maira", valor: 12, descricao: "Somente mais um usuário para ser apresentado nessa lista", tipoParametro: "Médio"}
                            ]);
                    } else {
                        resolve([
                            {id: 3, nome: "Caroil", valor: 53, descricao: "Outro usuário retornado do banco de dados", tipoParametro: "Baixo"},
                            {id: 4, nome: "José", valor: 31, descricao: "Mais um usuário para ser apresentado nessa lista", tipoParametro: "Médio"},
                            {id: 5, nome: "João", valor: 42, descricao: "Dados de usuário para ser exibido aqui", tipoParametro: "Alto"},
                            {id: 6, nome: "Gabi", valor: 7, descricao: "Apresentar informações de outro user", tipoParametro: "Médio"}
                        ]);
                    }
                });
        })

        return requisicaoDB;
    };
    //  -------------------------- paginatorNgTable --------------------------


    vm.listarParametro = function () {

        ParametroSistemaService
            .consultar(vm.model.filtros.descricao)
            .then(function (res) {
                vm.model.listaParametroSistema = res;
                vm.pesquisaRealizada = true;
            }, function () {
                vm.model.listaParametroSistema = [];
                vm.pesquisaRealizada = true;
            });
    };

    vm.selecionarParametro = function (item) {
        $state.go("parametro-sistema.editar", {id: item.id});
    };

    vm.cadastrarParametro = function () {
        vm.model.dadoItemEdicao = {};
        $state.go('parametro-sistema.cadastrar')
    };

    vm.editarParametro = function () {
        $scope.$broadcast('cs-validar', 'form');
    
        if ($scope.form.$valid) {
            var params = {
                id: vm.model.dadoItemEdicao.id,
                nome: vm.model.dadoItemEdicao.nome,
                valor: vm.model.dadoItemEdicao.valor,
                descricao: vm.model.dadoItemEdicao.descricao,
                tipoParametro: vm.model.dadoItemEdicao.tipoParametro
            };

            if ($state.params.id) {
                ParametroSistemaService
                    .editar($state.params.id, params)
                    .then(function () {
                        $state.go("parametro-sistema.voltar");
                        csMessageService.setMessage(MessageConstants.ALTERACAO_SUCESSO, csMessageService.INFO);
                        init();
                    });
            } else {
                ParametroSistemaService
                    .salvar(params)
                    .then(function () {
                        $state.go("parametro-sistema.voltar");
                        csMessageService.setMessage("Alteração realizada com sucesso", 0);
                        init();
                    });
            }
        }
    };

    vm.excluirParametro = function (item) {
        console.log("Excluir Parametro");
        console.log("item", item);

        ParametroSistemaService
            .excluir(item.id)
            .then(function () {
                $state.go("parametro-sistema.voltar");
                csMessageService.setMessage("Alteração realizada com sucesso", 0);
                vm.listarParametro();
            });
    };

    function init() {

        if ($state.params.id) {
            ParametroSistemaService
                .buscar($state.params.id)
                .then(function (response) {
                    vm.model.dadoItemEdicao = response.dados;
                });
        } else {
            vm.model.dadoItemEdicao = {};
            vm.pesquisaRealizada = false;
        }
    };

    init();

}

ParametroSistemaController.$inject = ['$scope', '$rootScope', 'Restangular', '$state',
    'ParametroSistemaService', 'ReportService', 'ParametroSistemaModel',
    'csMessageService', '$http', 'MessageConstants'];
export default ParametroSistemaController;
