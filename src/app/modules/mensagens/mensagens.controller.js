'use strict';

function MensagensController(
    $scope,
    $rootScope,
    Restangular,
    $state,
    $http,
    csMessageService,
    MessageConstants) {

    var vm = this;

    vm.exibirMensagemSucesso = function () {
        csMessageService.setMessage(MessageConstants.ALTERACAO_SUCESSO, csMessageService.SUCCESS);
    }

    vm.exibirMensagemErro = function () {
        csMessageService.setMessage(MessageConstants.ALTERACAO_ERRO, csMessageService.ERROR);
    }

    vm.exibirMensagemAlerta = function () {
        csMessageService.setMessage(MessageConstants.SALVO_SUCESSO, csMessageService.ALERT);
    }

    vm.exibirMensagemInformacao = function () {
        csMessageService.setMessage(MessageConstants.EXCLUSAO_SUCESSO, csMessageService.INFO);
    }
}

MensagensController.$inject = [
    '$scope',
    '$rootScope',
    'Restangular',
    '$state',
    '$http',
    'csMessageService',
    'MessageConstants'];
export default MensagensController;
