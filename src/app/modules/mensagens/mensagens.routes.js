'use strict';
import layoutTemplate from '../../assets/templates/layoutAdm.html';

import homeView from './views/mensagens.html';

function MensagensRoutes($urlRouterProvider, $stateProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('mensagens', {
            url: '/mensagens',
            templateUrl: layoutTemplate,
            abstract: true,
            reloadOnSearch: false,
            ncyBreadcrumb: {
                label: 'Mensagens'
            },
            resolve: {
                //Lazy loading do modulo
                loadModule: ($q, $ocLazyLoad) => {
                    return $q((resolve) => {
                        require.ensure([], () => {
                            let module = require('./mensagens.module').default;
                            $ocLazyLoad.load({name: module.name});
                            resolve(module.name);
                        });
                    });
                }
            }
        })
        .state('mensagens.exemplo', {
            url: '/exemplo',
            templateUrl: homeView,
            parent: 'mensagens',
            controller: 'MensagensController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        })
        .state('mensagens.voltar', {
            url: '/',
            templateUrl: homeView,
            parent: 'mensagens',
            controller: 'MensagensController as vm',
            ncyBreadcrumb: {
                label: 'Exemplo'
            },
            resolve: {
                user: function(grant){
                    return grant.only([
                        {test: 'isAuthenticated', state: 'auth.login'},
                        {test: 'isNotLocked', state: 'auth.lock'},
                    ]);
                }
            }
        });
}

MensagensRoutes.$inject = ['$urlRouterProvider', '$stateProvider'];

export default angular
    .module('mensagens.routes', [])
    .config(MensagensRoutes);