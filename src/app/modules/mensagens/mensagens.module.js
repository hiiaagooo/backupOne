'use strict';
import bootstrap from './mensagens.run';

import MensagensController from './mensagens.controller';

// import ModalDirective from '../../directives/modal/modal.module';

/*
 * Start the module with dependencies
 * */
let module = angular.module('modal', [

    // ModalDirective.name
]);
/*
 * Module bootstrap
 * */
module.run(bootstrap);

/*
 * Start the controllers
 * */
module.controller('MensagensController', MensagensController);

export default module;
