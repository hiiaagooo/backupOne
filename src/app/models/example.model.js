'use strict';

function ExampleModel(Restangular) {
    var vm = this;

    vm.service = Restangular.withConfig(function(configurer){
        configurer.setBaseUrl(process.env.REST_BASE + 'api');
    }).service('example');

    return vm.service;
}

ExampleModel.$inject = ['Restangular'];

export default angular.module('app.models.ExampleModel', [])
    .service('ExampleModel', ExampleModel);